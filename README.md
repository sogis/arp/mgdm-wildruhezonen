# MGDM Wildruhezonen

```
docker run --rm --name editdb -p 54321:5432 -e POSTGRES_PASSWORD=mysecretpassword -e POSTGRES_DB=edit -e PG_READ_PWD=dmluser -e PG_WRITE_PWD=ddluser sogis/oereb-db:latest

```

```
java -jar /Users/stefan/apps/ili2pg-4.6.0/ili2pg-4.6.0.jar --dbhost localhost --dbport 54321 --dbdatabase edit --dbusr ddluser --dbpwd ddluser --nameByTopic --strokeArcs --defaultSrsCode 2056 --createEnumTabs --createUnique --createFk --createFkIdx --models "Wildruhezonen_Codelisten_V2_1;Wildruhezonen_LV95_V2_1" --dbschema arp_wildruhezonen --schemaimport
```

```
java -jar /Users/stefan/apps/ili2pg-4.6.0/ili2pg-4.6.0.jar --dbhost localhost --dbport 54321 --dbdatabase edit --dbusr ddluser --dbpwd ddluser --models "Wildruhezonen_Codelisten_V2_1" --dbschema arp_wildruhezonen --import Wildruhezonen_Catalogues_V2_1.xml
```

```
java -jar /Users/stefan/apps/ili2pg-4.6.0/ili2pg-4.6.0.jar --dbhost localhost --dbport 54321 --dbdatabase edit --dbusr ddluser --dbpwd ddluser --models "Wildruhezonen_LV95_V2_1" --dbschema arp_wildruhezonen --export fubar.xtf
```

```
curl -u xx:YYY -F topic=wildruhezonen_v2_1 -F lv95_file=@fubar.xtf.zip -F publish=true "https://geodienste.ch/data_agg/interlis/import"

```